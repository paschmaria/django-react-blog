from .base import *

ALLOWED_HOSTS = ['*']

CORS_ORIGIN_WHITELIST = ("https://yourproductionurl.com",)
