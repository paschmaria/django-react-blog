from django.contrib.auth.models import User
from django.db import models
from taggit.managers import TaggableManager


class Post(models.Model):
    """
    Instance of created post
    """

    author = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=250)
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    tags = TaggableManager()

    class Meta:
        ordering = ("created",)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return f"/posts/{self.id}"
