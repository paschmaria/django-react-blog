from django.contrib.postgres.search import SearchQuery, SearchRank, SearchVector

from .models import Post


def get_similar_posts(query):
    """
    Get posts with characters matching the search query
    """

    search_vector = SearchVector("title")
    search_query = SearchQuery(query)
    results = (
        Post.objects.annotate(
            search=search_vector, rank=SearchRank(search_vector, search_query)
        )
        .filter(search=search_query)
        .order_by("-rank")
    )
    return results
