from django.test import TestCase

from model_mommy import mommy


class TestPost(TestCase):
    """
    Unit test for the Post model
    """

    def setUp(self):
        self.model = mommy.make("api.Post")

    def test_str(self):
        self.assertEquals(str(self.model), self.model.title)

    def test_url(self):
        id = self.model.id
        model_url = self.model.get_absolute_url()
        self.assertURLEqual(model_url, f"/posts/{id}")
