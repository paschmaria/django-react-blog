import json
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

from model_mommy import mommy

from ..serializers import PostSerializer


class TestPost(TestCase):
    """
    Unit test for the Post model
    """

    def setUp(self):
        self.client = APIClient()
        self.user_model = mommy.make("auth.User")
        self.post_models = mommy.make("api.Post", author=self.user_model, _quantity=3)

    def test_create_new_post_if_valid(self):
        """
        Test that POST request creates new post
        """
        data = {
            "userId": self.user_model.id,
            "title": "Some title",
            "body": "Here's some Lorem Ipsum text for testing",
            "tags": ["new", "lorem"],
        }
        response = self.client.post("/api/v1/posts/", data=data, format="json")
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)

    def test_get_all_posts(self):
        """
        Test that get request without parameters retrieves all posts
        """
        response = self.client.get("/api/v1/posts/")
        posts = self.post_models
        serializer = PostSerializer(posts, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_get_post_using_query_params(self):
        """
        Test that query parameters returns post(s) if found
        """
        response = self.client.get(
            "/api/v1/posts/", data={"query": self.post_models[0].title}
        )
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_get_valid_single_post(self):
        """
        Test that fetching existing post using id retrieves post
        """
        response = self.client.get(
            "/api/v1/posts/", kwargs={"pk": self.post_models[0].pk}
        )
        post = self.post_models[0]
        serializer = PostSerializer(post)
        self.assertEqual(dict(response.data[0]), serializer.data)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_post(self):
        response = self.client.get("/api/v1/posts/", kwargs={"pk": 500})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
