from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from .models import Post
from .serializers import PostSerializer
from .utils import get_similar_posts


class PostViewset(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    # permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        query = self.request.query_params.get("query")

        if query is not None:
            queryset = get_similar_posts(query)
            return queryset

        return super().get_queryset()
