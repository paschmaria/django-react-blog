from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django_seed import Seed
from django_seed.exceptions import SeederCommandError

from api.models import Post


class Command(BaseCommand):
    help = "Seed API database with fake data"

    def add_arguments(self, parser):

        help_text = "number of each model to seed (default 10)"
        parser.add_argument(
            "--number", dest="number", type=int, default=10, help=help_text
        )

    def handle(self, *args, **options):

        try:
            number = int(options["number"])
        except ValueError:
            raise SeederCommandError("The value of --number must be an integer")

        seeder = Seed.seeder()
        User = get_user_model()
        models = [User, Post]

        for model in models:
            seeder.add_entity(model, number)
            print("Seeding %i %ss" % (number, model.__name__))

        generated = seeder.execute()

        for model, pks in generated.items():
            for pk in pks:
                print(
                    "Model {} generated record with primary key {}".format(
                        model.__name__, pk
                    )
                )
