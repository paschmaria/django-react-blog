from django.contrib.auth import get_user_model
from rest_framework import serializers, exceptions
from taggit_serializer.serializers import TagListSerializerField, TaggitSerializer

from .models import Post

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    """
    Serializers the User model
    """

    class Meta:
        model = User
        fields = ("id",)


class PostSerializer(TaggitSerializer, serializers.ModelSerializer):
    """
    Serializers the Post Model into JSON format
    """

    user = UserSerializer(read_only=True)
    userId = serializers.IntegerField(write_only=True)
    url = serializers.CharField(source="get_absolute_url", read_only=True)
    tags = TagListSerializerField()

    class Meta:
        model = Post
        fields = (
            "id",
            "user",
            "userId",
            "url",
            "title",
            "body",
            "created",
            "modified",
            "tags",
        )

    def create(self, validated_data):
        userId = validated_data.pop("userId")

        try:
            author = User.objects.get(pk=userId)
        except User.DoesNotExist:
            raise exceptions.NotFound(f"Author with id {userId} does not exist!")
        
        post = Post.objects.create(author_id=author.pk, **validated_data)
        return post
