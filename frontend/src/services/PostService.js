import http from "../http-common";


class PostService {
    async getSinglePost(pk) {
        const url = `/posts/${pk}/`;
        const response = await http.get(url);
        return response.data;
    }

    async getPostsByTitle(query) {
        const url = `/posts/?query=${query}`;
        const response = await http.get(url);
        return response.data;
    }

    async getAllPosts() {
        const url = `/posts/`;
        const response = await http.get(url);
        return response.data;
    }

    async createPost(post) {
        const url = `/posts/`;
        const response = await http.post(url, post);
        return response.data;
    }
}

export default PostService;