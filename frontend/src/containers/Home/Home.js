import React, { Component } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import Footer from '../../components/Footer/Footer';
import Form from '../../components/Form/Form';
import Navbar from '../../components/Navbar/Navbar';


const styles = theme => ({
    heroContent: {
        padding: theme.spacing(16, 0, 6),
        height: '100%',
    },
});


class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            query: '',
            redirectToPostPage: false,
        };
        
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    handleInputChange(e) {
        this.setState({
            query: e.target.value
        })
    }

    handleFormSubmit(e) {
        e.preventDefault();
        let queryString = "query=" + this.state.query;
        this.props.history.push({pathname: `/posts?${queryString}`});
        // this.props.history.push(`/posts?${queryString}`);
    }

    render() {
        const classes = this.props.classes;
    
        return (
            <React.Fragment>
                <CssBaseline />
                <Navbar url="/posts" btnText="View all Posts" />
                <main>
                    <div className={classes.heroContent}>
                        <Container maxWidth="sm">
                            <Typography
                                component="h1"
                                variant="h2"
                                align="center"
                                color="textPrimary"
                                gutterBottom
                            >
                                Search for Post
                            </Typography>
                            <Form
                                change={this.handleInputChange}
                                submit={this.handleFormSubmit}
                            />
                        </Container>
                    </div>
                </main>
                <Footer />
            </React.Fragment>
        );
    }
}

export default withStyles(styles)(Home);