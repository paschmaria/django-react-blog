import React, { Component } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Alert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
import CloseIcon from '@material-ui/icons/Close';

import Navbar from '../../components/Navbar/Navbar';
import Card from '../../components/CardHolder/Card/Card';
import CardHolder from '../../components/CardHolder/CardHolder';
import PostService from '../../services/PostService';
import Footer from '../../components/Footer/Footer';


const postservice = new PostService();

const styles = theme => ({
    heroContent: {
        padding: theme.spacing(16, 0, 6),
        height: '100%',
    }
});

class Posts extends Component {

    state = {
        query: this.props.location.search,
        posts: [],
        connError: false,
        errorMessage: '',
        alertOn: false
    };

    componentDidMount() {
        const qs = new URLSearchParams(this.state.query)
        const query = qs.get('query')

        if (query && query.length > 0) {
            postservice.getPostsByTitle(query)
            .then(result => {
                this.setState({
                    posts: result.data,
                })
            })
            .catch(e => {
                this.setState({
                    connError: true,
                    alertOn: true,
                    errorMessage: e.message,
                })
            })
        } else {
            postservice.getAllPosts()
            .then(result => {
                this.setState({
                    posts: result,
                })
            })
            .catch(e => {
                this.setState({
                    connError: true,
                    alertOn: true,
                    errorMessage: e.message,
                })
            })
        }
    }

    closeAlert() {
        this.setState({
            alertOn: false
        })
    }

    render() {
        const classes = this.props.classes;
        let posts = null;

        if (
            Array.isArray(this.state.posts) &&
            this.state.posts.length
        ) {
            posts = <CardHolder posts={this.state.posts} />;
        } else {
            posts = <Card isEmpty={true} />;
        }

        return (
            <React.Fragment>
                <CssBaseline />
                <Navbar url="/" btnText="Back to search" />
                <main>
                    <div className={classes.heroContent}>
                        <Container maxWidth="sm">
                            {this.state.connError ? <Collapse in={this.state.alertOn}>
                                <Alert
                                severity="warning"
                                action={
                                    <IconButton
                                    aria-label="close"
                                    color="inherit"
                                    size="small"
                                    onClick={() => this.closeAlert()}
                                    >
                                    <CloseIcon fontSize="inherit" />
                                    </IconButton>
                                }
                                >
                                    {this.state.errorMessage}
                                </Alert>
                            </Collapse> : null}
                            <Typography
                                component="h1"
                                variant="h2"
                                align="center"
                                color="textPrimary"
                                gutterBottom
                            >
                                Posts
                            </Typography>
                            {posts}
                        </Container>
                    </div>
                </main>
                <Footer />
            </React.Fragment>
        )
    }
}

export default withStyles(styles)(Posts);