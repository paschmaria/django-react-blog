import React from 'react';
import {
	BrowserRouter as Router,
	Route,
	Switch
} from "react-router-dom";
import Home from './Home/Home';
import Posts from './Posts/Posts';


function App() {
	return (
		<Router>
			<Switch>
				<Route exact path="/" component={Home} />
				<Route path="/posts" component={Posts} />
			</Switch>
		</Router>
	);
}

export default App;
