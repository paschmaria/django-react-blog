import React from 'react';
import Card from './Card/Card';


const PostCardHolder = ({ posts }) => {    
    return posts.map((post, index) => {
        return <Card post={post} key={index} />
    })
}

export default PostCardHolder;