import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';


const useStyles = makeStyles(theme => ({
    root: {
        marginBottom: theme.spacing(3),
    },
    header: {
        paddingBottom: 0,
    },
    empty: {
        textAlign: 'center',
        marginTop: theme.spacing(2),
    },
    pos: {
        marginBottom: 12,
    },
}));

const PostCard = ({ post, isEmpty=false }) => {
    const classes = useStyles();

    const getDateTime = datetime => {
        let dt = new Date(datetime);
        return dt.toLocaleString();
    }

    const truncateString = (str, num) => {
        if (str.length <= num) {
          return str
        }
        return str.slice(0, num) + '...'
    }
    
    return (
        <Card className={classes.root}>
            {!isEmpty ? 
                <React.Fragment>
                    <CardHeader
                        title={post.title}
                        subheader={`Updated on: ${getDateTime(post.modified)}`}
                        className={classes.header}
                    />
                    <CardContent>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {truncateString(post.body, 100)}
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button href={post.url} size="small" color="primary">
                            Read
                        </Button>
                    </CardActions>
                </React.Fragment>
            : <p className={classes.empty}>No Posts found!</p>}
        </Card>
    )
}

export default PostCard;