import React from "react";
import { Link } from "react-router-dom";
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles(theme => ({
    title: {
        flexGrow: 1,
    },
}));

const Navbar = ({url, btnText}) => {
    const classes = useStyles();

    return ( 
        <AppBar>
            <Toolbar>
                <Typography variant="h6" color="inherit" className={classes.title}>
                    Get Posts App
                </Typography>
                <Button component={Link} to={url} color="inherit">{btnText}</Button>
            </Toolbar>
        </AppBar>
    );
}

export default Navbar;
