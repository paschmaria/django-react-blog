import React from 'react';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import Search from '@material-ui/icons/Search';


const useStyles = makeStyles(theme => ({
    margin: {
      margin: theme.spacing(1),
    },
    form: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'column',
    }
}));


const Form = ({ change, submit }) => {
    const classes = useStyles();

    return (
        <form className={classes.form} onSubmit={submit}>
            <FormControl className={classes.margin} required={true}>
                <InputLabel htmlFor="query">Type search keyword</InputLabel>
                <Input
                    id="query"
                    required={true}
                    onChange={change}
                    startAdornment={
                        <InputAdornment position="start">
                            <Search />
                        </InputAdornment>
                    }
                />
            </FormControl>
            <Button type="submit" variant="contained" color="primary">
                Search
            </Button>
        </form>
    )
};

export default Form;